'''
Created on Nov 19, 2016

'''

import numpy
import math
from random import sample

if __name__ == '__main__':
    pass

def Distance(instance1, instance2, length):
    distance = 0
    for x in range(4,length):
        if (x == 4):
            distance += pow((instance1[x]*10 - instance2[x]*10), 2)
        elif (x == 5):
            distance += pow((instance1[x]/1000 - instance2[x]/1000), 2)
        else:
            distance += pow((instance1[x] - instance2[x]), 2)
            
    return math.sqrt(distance)

path = 'data/HDI.csv'
first=True 
labeled = []
unlabeled = []
dataset = []
l = 0
u = 0
atual = 0
perc = 0.2
poss = sample(range(175), math.floor(175*perc))
print(poss)
file = open(path, 'r')
data = file.readline()
while data: 
    data = data.split(";")
    if first:        
        first = False
    else:
        i_data = []
        if  len(data)>2:
            klass = data[2]
            for i in range (math.floor(175*perc)):
                if (poss[i] == atual):
                    klass = "?"
            atual+=1        
            dataset.append([int(data[0]),data[1],klass,float(data[3]),float(data[4]),float(data[5]),float(data[6]),float(data[7]),float(data[8]),float(data[9])])
            if (klass == "?"):
                unlabeled.append([int(data[0]),data[1],klass,float(data[3]),float(data[4]),float(data[5]),float(data[6]),float(data[7]),float(data[8]),float(data[9])])
                u+=1
            else:
                labeled.append([int(data[0]),data[1],klass,float(data[3]),float(data[4]),float(data[5]),float(data[6]),float(data[7]),float(data[8]),float(data[9])])
                l+=1
        else:
            data        
    data = file.readline()

#Definindo as matrizes
#Matriz Y com (vh,h) = 1 e (m,l) = -1
Y = [[0 for x in range(l)] for y in range(l)]
for i in range(l):
    for j in range(l):
        if (i==j):
            if (labeled[i][2] == 'vh' or labeled[i][2] == 'h'):
                Y[i][j] = 1
            elif (labeled[i][2] == 'm' or labeled[i][2] == 'l'):
                Y[i][j] = -1
        else:
            Y[i][j] = 0

#print( "%s" % (str(Y)))

#Matriz K usando Kernel Gaussiano e Theta = 4
K = [[0 for x in range(l+u)] for y in range(l+u)]
for i in range(l+u):
    for j in range(l+u):
        K[i][j] = math.exp(-(Distance(dataset[i], dataset[j], 10)/4))
        
#Matriz W
W = [[0 for x in range(l+u)] for y in range(l+u)]
for i in range(l+u):
    for j in range(l+u):
        W[i][j] = Distance(dataset[i], dataset[j], 10)
        
#print( "%s" % (str(W[0])))

#Matriz J
J = [[0 for x in range(l+u)] for y in range(l)]
for i in range(l):
    for j in range(l+u):
        if (i==j):
            J[i][j] = 1
        else:
            J[i][j] = 0
            
#print( "%s" % (str(len(J[0]))))
            
#Matriz D
D = [[0 for x in range(l+u)] for y in range(l+u)]
for i in range(l+u):
    for j in range(l+u):
        if (i==j):
            for k in range(l+u):
                D[i][j] += W[i][k]

#Matriz L
L = [[0 for x in range(l+u)] for y in range(l+u)]
for i in range(l+u):
    for j in range(l+u):
        L[i][j] = D[i][j] - W[i][j]

#Matriz Q, GammaA =  0.03125, GammaI = 1.0
#Chamar a expressao recorrente de expRec
Q = [[0 for x in range(l+u)] for y in range(l+u)]
temp = [[0 for x in range(l+u)] for y in range(l+u)]

I = numpy.identity(l+u)
temp = numpy.dot(Y, J)
temp = numpy.dot(temp, K)
expRec = numpy.linalg.inv(numpy.add(numpy.dot(2*0.03125,I), numpy.dot(2*(1/pow(u+l,2)),numpy.dot(L,K))))
expRec = numpy.dot(expRec, numpy.dot(numpy.transpose(J), Y))
Q = numpy.dot(temp, expRec)
print( "%s" % (str(Q)))

#print( "%s" % (str(labeled)))    
#print( "%s" % (str(unlabeled)))


